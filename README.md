# vimimus-configius

My Vim settings, hacks and plugins.

## Third party Plugins

* [command-t](https://github.com/wincent/command-t) - Open files/buffers with fuzzy search
* [tagbar](https://github.com/majutsushi/tagbar) - Sidebar for ctags
* [vim-vinegar](https://github.com/tpope/vim-vinegar) - Enhanced Netrw
* [vim-gitgutter](https://github.com/airblade/vim-gitgutter) - Shows git diff in the gutter
* [vim-fugitive](https://github.com/tpope/vim-fugitive) - Git wrapper

## Dependencies
Use the package manager in Linux/Mac to install, or the provided links for Windows.

**Ruby 2.4.5**

Required for command-t

Mac/Linux: https://www.ruby-lang.org/en/downloads/

Windows: https://rubyinstaller.org/downloads/

**C compiler**

Required for Command-T
MinGW for example as part of MSYS2: https://www.msys2.org/

**Universal Ctags or Exuberant Ctags** 

Required forTagbar

Windows: https://github.com/universal-ctags/ctags-win32

**Git**

Required for vim-gitgutter and vim-fugitive

Windows: https://git-scm.com/download/win

## Intallation on Linux or Mac
It may be a good idea to backup your current .vim (alt. vimfiles) directory, and .vimrc before you begin. 

**Clone the repo (Linux/Mac):**
```
cd ~
git clone --recursive https://mapel@bitbucket.org/mapel/vimimus-configius.git .vim
```

**Clone the repo (Windows):**
```
cd ~
git clone --recursive https://mapel@bitbucket.org/mapel/vimimus-configius.git vimfiles
```
**Download the submodules (if you missed the --recursive flag when cloning):**
```
git submodule update --init
```
**Compile the Ruby extensions for the command-t plugin:**
```
cd ~/.vim/pack/plugins/start/command-t/ruby/command-t/ext/command-t
ruby extconf.rb
make
```
**Generate helptags for all plugins:**
```
sudo vim
:helptags ALL
```
