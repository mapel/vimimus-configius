" Some useful hacks

nnoremap <leader>f :call <SID>FoldColumnToggle()<cr>
nnoremap <leader>q :call <SID>ToggleQuickfixWin()<cr>
nnoremap <leader>d :call <SID>ToggleDim()<cr>
nnoremap <leader>g :set operatorfunc=<SID>GrepOperator<cr>g@
vnoremap <leader>g :<c-u>call <SID>GrepOperator(visualmode())<cr>

let g:quickfix_is_open = 0

function! s:GrepOperator(type)
   let saved_unnamed_register = @@

   if a:type ==# 'v'
      normal! `<v`>y
   elseif a:type ==# 'char'
      normal! `[v`]y
   else
      return
   endif

   silent execute "grep! -sR " . shellescape(@@) . " ."
   copen 15
   redraw!
   let g:quickfix_is_open = 1

   let @@ = saved_unnamed_register
endfunction

function! s:FoldColumnToggle()
   if &foldcolumn
      setlocal foldcolumn=0
   else
      setlocal foldcolumn=4
   endif
endfunction

function! s:ToggleQuickfixWin()
   if g:quickfix_is_open
      execute "normal! \<C-W>\<C-P>"
      cclose
      let g:quickfix_is_open = 0
   else
      copen 15
      let g:quickfix_is_open = 1
   endif
endfunction

if &background == "dark"
   let g:dim_colorscheme = 1
else
   let g:dim_colorscheme = 0
endif

function! s:ToggleDim()
   if g:dim_colorscheme 
      colorscheme morning
      let g:dim_colorscheme = 0
   else
      colorscheme desert
      let g:dim_colorscheme = 1
   endif
endfunction
