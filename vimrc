" Magnus Pellijeff .vimrc

" General ------------------------------------------------------------------ {{{

syntax on
set t_Co=256            " Enable 256 colours in Vim
colorscheme desert
set foldlevelstart=0    "0: All folds closed, 1: some closed, 99: none closed
set foldmethod=syntax   " Folding based on syntax items with 'fold' argument
set nofoldenable        " Folding is not on by default
set noswapfile          " New buffers will be loaded without a swap file
set winminheight=0      " Minimum window height will only display the status line
set number              " Line numbers
set diffopt+=vertical   " Start diff mode in vertical split
set mouse=a             " Enable mouse for all modes
set timeoutlen=700      " Key map delay in ms
set ttimeoutlen=0       " Key code delay in ms
set autowrite           " Automatically save file before certain commands
" }}}

" Statusline --------------------------------------------------------------- {{{

set laststatus=2        "Always show statusline
set statusline=\[%n]    "Buffer number
set statusline+=\ %.30f "File name, and relative file path, maxwidth 30 chars"
set statusline+=\ %h    "Help buffer flag
set statusline+=\ %m    "Modifiable flag
set statusline+=%<      "Truncation point if too long
set statusline+=\ %r    "Read only flag"
set statusline+=%=      "Separation point between left/right aligned items
set statusline+=%{tagbar#currenttag('[%s]','')} "Display current tag if available
set statusline+=\ %l    "Line number
set statusline+=/%L     "Number of lines in the buffer
set statusline+=,       "Separator
set statusline+=%c      "Column
set statusline+=%V      "Virtual column
set statusline+=\ %P    "Percentage through file
" }}}

" Edit --------------------------------------------------------------------- {{{

set shiftwidth=4  " Number of spaces to use for autoindent, >>, <<
set softtabstop=4 " Number of characters moved when the <TAB> or <BS> keys are pressed
set expandtab     " Expands <TAB> into softabstop spaces, use spaces for autoindent, '<' and '>'
set showmatch
set matchtime=1
set shiftround    " Round indent when using < or > to multiples of shiftwidth
set autoindent    " Copy indent from current line when starting a new line
set updatetime=500
" set tabstop=8 "If expandtab is unset and tabstop is  different from softtabstop, the number of
" spaces inserted will be minimized, e.g. the maximum number of tabs will be
" inserted

" Operator: inside email (needs tweaking for emails in side parantheses e.g.)
onoremap in@ :<c-u>execute "normal! /[a-z0-9.]\\+@\\w\\+.\\w\\{2,4}\r:noh\rvE"<cr>
" }}}

" Search ------------------------------------------------------------------- {{{

let @/ = "" "Clear the last search pattern
set hlsearch "Highlight search pattern
set ignorecase " Case insensitive search
set incsearch " Highlight typed string dynamically
set wildignore+=cscope.*
set path+=** "Search through every subdirectory of every subdirectory
set wildmenu "<Tab> invokes command-line completion, with displayed matches
" }}}

" Pane switching ----------------------------------------------------------- {{{

noremap <silent> <c-k> :wincmd k<CR>
noremap <silent> <c-j> :wincmd j<CR>
noremap <silent> <c-h> :wincmd h<CR>
noremap <silent> <c-l> :wincmd l<CR>
noremap <silent> <c-up> :wincmd k<CR>
noremap <silent> <c-down> :wincmd j<CR>
noremap <silent> <c-left> :wincmd h<CR>
noremap <silent> <c-right> :wincmd l<CR>
" }}}

" Custom keymaps ----------------------------------------------------------- {{{

let mapleader = '\'
let maplocalleader = "\\"

" Save the current file if it has been changed
nnoremap <silent><Leader>s :update<CR>

" Toggle relative number
nnoremap <leader>R :setlocal relativenumber!<CR>

" Clear highlight
nnoremap <Leader><space> :noh<CR>

" Execute current file
nnoremap <F9> :!%:p <Enter>

" Edit .vimrc in the current pane
nnoremap <leader>ev :e $MYVIMRC<cr>

" Close tagbar and do a vertical split and edit vimrc
nnoremap <leader>vv :TagbarClose<cr> :vsplit $MYVIMRC<cr>

" Split and do a horizontal split and edit of vimrc
nnoremap <leader>hv :split $MYVIMRC<cr>

" Edit .vimrc in a new tab
nnoremap <leader>tv :tabe $MYVIMRC<cr>

" Wrap word under cursor with double quotes
nnoremap <leader>" viw<Esc>bi"<Esc>ea"<Esc>
vnoremap <leader>" <Esc>`>a"<Esc>`<i"<Esc>v

" Wrap word under cursor with single quotes
nnoremap <leader>' viw<esc>bi'<esc>ea'<esc>
vnoremap <leader>' <Esc>`>a'<Esc>`<i'<Esc>v

" Open previous buffer in a split below
nnoremap <leader>pb :execute "rightbelow split " . bufname("#")<CR>

" Highlight trailing spaces
nnoremap <leader>w :match Error /\v\s+$/<CR>

" Clear match highlight
nnoremap <leader>W :match<CR>

" }}}

" Auto commands " ---------------------------------------------------------- {{{

augroup TagsFiles
   autocmd!
   autocmd BufEnter /* call LoadCscope()
augroup END

augroup Vimimus
   autocmd!
   "Always open quickfix at the bottom
   autocmd FileType qf wincmd J
   " Go to function head from function body
   autocmd FileType c  nnoremap <buffer> <localleader>[ [[?\w(<CR>b:let @/=""<CR>
   autocmd FileTYpe sh nnoremap <buffer> <localleader>[ [{?B<CR>
   " Go to function name from argument list
   autocmd Filetype c  nnoremap <buffer> <localleader>( ?\w(<CR>b:let @/=""<CR>
   " Auto complete
   autocmd FileType c,cpp,javascript   :iabbrev <buffer> iff if ()<left>
   autocmd FileType python             :iabbrev <buffer> iff if:<left>
   " Enable highlighting of txt files 
   autocmd FileType text set filetype=conf
augroup END

augroup CommentLine
   autocmd!
   autocmd FileType c            nnoremap <buffer> <localleader>c I/*<esc>A*/<esc>
   autocmd FileType cpp          nnoremap <buffer> <localleader>c I//<esc>
   autocmd FileType javascript   nnoremap <buffer> <localleader>c I//<esc>
   autocmd FileType python       nnoremap <buffer> <localleader>c I#<esc>
   autocmd FileType sh           nnoremap <buffer> <localleader>c I#<esc>
   autocmd FileType vim          nnoremap <buffer> <localleader>c I"<esc>
augroup END

augroup filetype_markdown
   au!
   " Operator: inside header
   au FileType markdown onoremap <buffer> ih :<c-u>execute "normal! ?^\\([=-]\\)\\1\\+$\r:noh\rkvg_"<cr>
   " Operator: around header
   au FileType markdown onoremap <buffer> ah :<c-u>execute "normal! ?^[=-][=-]\\+$\r:noh\rg_vk0"<cr>
augroup END

augroup filetype_vim
   autocmd!
   autocmd FileType vim setlocal foldmethod=marker
   autocmd FileType vim setlocal foldenable
   autocmd BufWritePost $MYVIMRC source $MYVIMRC
augroup END
" }}}

" Auto correct ------------------------------------------------------------- {{{

iabbrev adn and
iabbrev waht what
iabbrev tehn then
iabbrev teh the
iabbrev tow two
iabbrev ccopy Copyright 2018 Magnus Pellijeff, all rights reserved
" }}}

" ==============================================================================
" Plugins
" ==============================================================================

" Netrw -------------------------------------------------------------------- {{{

set nocp
filetype plugin on
let g:netrw_fastbrowse=0 "0 (no caching)
let g:netrw_keepdir=1 "Current dir is immune from the browsing dir
let g:netrw_preview=1 "Use vertical splitting for previews
"let g:netrw_winsize=30
let g:netrw_special_syntax=1
"let g:netrw_chgwin=2
"let g:netrw_browse_split=0
" }}}

" Command-T ---------------------------------------------------------------- {{{

nmap <silent> <leader>l <Plug>(CommandTLine)
" }}}

" Tagbar ------------------------------------------------------------------- {{{

let g:tagbar_width=75
let g:tagbar_zoomwidth = 0
let g:tagbar_sort=0
let g:tagbar_compact=1
let g:tagbar_indent=1
noremap <F8> :TagbarToggle<CR>
highlight TagbarHighlight ctermfg=red

" Open Tagbar pane when only one window is open
nnoremap <c-w>o :on<CR>:TagbarToggle<CR>
" }}}

" Tags --------------------------------------------------------------------- {{{

set tags=tags;/ " Autoload ctags DB

" Autoload Cscope DB

function! LoadCscope()
   let db = findfile("cscope.out", ".;")
   if (!empty(db))
      let path = strpart(db, 0, match(db, "/cscope.out$"))
      set nocscopeverbose " suppress 'duplicate connection' error
      exe "cs add " . db . " " . path
      set cscopeverbose
      set cscopequickfix=s-,c-,d-,i-,t-,e-,a- " C-T or C-h does not play well
      set cscopetag
      set cscopetagorder=0 " 0: cscope first, 1: ctags first
      set cscopepathcomp=3 " 0: entire path, 1: filename only, n: n components
   elseif $CSCOPE_DB != ""
      cs add $CSCOPE_DB
   endif
endfunction

" Cscope: Custom key bindings ---------------------------------------------- {{{

noremap <C-_>s :cs find s <C-R>=expand("<cword>")<CR><CR>
noremap <C-_>g :cs find g <C-R>=expand("<cword>")<CR><CR>
noremap <C-_>c :cs find c <C-R>=expand("<cword>")<CR><CR>
noremap <C-_>t :cs find t <C-R>=expand("<cWORD>")<CR><CR>
noremap <C-_>e :cs find e <C-R>=expand("<cword>")<CR><CR>
noremap <C-_>f :cs find f <C-R>=expand("<cfile>")<CR><CR>
noremap <C-_>i :cs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
noremap <C-_>d :cs find d <C-R>=expand("<cword>")<CR><CR>
noremap <C-_>" yi"<Esc>:cs find t <C-R>"<CR>
noremap <C-_>' yi'<Esc>:cs find t <C-R>"<CR>
" }}}

" Cscope: Split horizontally with search results in new window ------------- {{{

noremap <C-Space>s :scs find s <C-R>=expand("<cword>")<CR><CR>
noremap <C-Space>g :scs find g <C-R>=expand("<cword>")<CR><CR>
noremap <C-Space>c :scs find c <C-R>=expand("<cword>")<CR><CR>
noremap <C-Space>t :scs find t <C-R>=expand("<cword>")<CR><CR>
noremap <C-Space>e :scs find e <C-R>=expand("<cword>")<CR><CR>
noremap <C-Space>f :scs find f <C-R>=expand("<cfile>")<CR><CR>
noremap <C-Space>i :scs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
noremap <C-Space>d :scs find d <C-R>=expand("<cword>")<CR><CR>
" }}}

" Cscope: Split vertically with search result in new window ---------------- {{{

noremap <C-Space><C-Space>s :vert scs find s <C-R>=expand("<cword>")<CR><CR>
noremap <C-Space><C-Space>g :vert scs find g <C-R>=expand("<cword>")<CR><CR>
noremap <C-Space><C-Space>c :vert scs find c <C-R>=expand("<cword>")<CR><CR>
noremap <C-Space><C-Space>t :vert scs find t <C-R>=expand("<cword>")<CR><CR>
noremap <C-Space><C-Space>e :vert scs find e <C-R>=expand("<cword>")<CR><CR>
noremap <C-Space><C-Space>i :vert scs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
noremap <C-Space><C-Space>d :vert scs find d <C-R>=expand("<cword>")<CR><CR>
noremap <C-_>v" yi"<Esc>:vert scs find t <C-R>"<CR>
noremap <C-_>v' yi'<Esc>:vert scs find t <C-R>"<CR>
" }}}
" }}}

